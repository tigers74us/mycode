#!/usr/bin/python3
## Try a real world test with getpass

## import Paramiko so we can talk SSH
import paramiko # allows Python to ssh
import os # low level operating system commands
import glob
import getpass # we need this to accept passwords

def main():
    ## where to connect to
    t = paramiko.Transport("10.10.2.3", 22) ## IP and port of bender
    
    ## how to connect (see other labs on using id_rsa private / public keypairs)
    t.connect(username="bender", password=getpass.getpass()) # notice the password references getpass
    
    ## Make an SFTP connection object
    sftp = paramiko.SFTPClient.from_transport(t)

    ## File directory
    path=os.path.expanduser('/home/student/mycode/paramikosftp/')
    
    ## copy our files script to bender
    ## iterate across the files within directory
    for x in [item for item in os.listdir(path) if not item.endswith(('.txt','html'))  ]: # iterate on directory contents
      #if not os.path.isdir(path+x): # filter everything that is NOT a directory
        #if not [item for item in os.path.isdir("/home/student/mycode/paramikosftp/"+x) if item.endswith(".py")]: # filter out file with .py extension.
          sftp.put("/home/student/mycode/paramikosftp/"+x, "/home/bender/"+x) # move file to target location
    
    ## close the connection
    sftp.close() # close the connection
    t.close()

if __name__ == "__main__":
    main()
