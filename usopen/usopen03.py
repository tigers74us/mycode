#!/usr/bin/env python3
## USOpen Tournament Switch Checker -- 2018.05.01
''' usopen.py
This script is being designed to provide the following automated tasks:
- ping check the router (import os)
- login check the router (import netmiko)
- determine if interfaces in use are up (import netmiko)
- Apply new configuration (import netmiko) # not yet built

The IPs and device type should be made available via an Excel spreadsheet

'''
import os

## pyexcel and pyexce-xls are required for our program to execute
# python3 -m pip install --user pyexcel
# python3 -m pip install --user pyexcel-xls
import pyexcel


from datetime import datetime

# python3 -m pip install --user netmiko
from netmiko import ConnectHandler

# import bootstrapper
import bootstrapper

def retv_excel(par):
    d = {}
    records = pyexcel.iget_records(file_name=par)
    for record in records:
        d.update( { record['hostname'] : record['driver'] } )
    return d

def login_router(dev_type, dev_ip, dev_un, dev_pw):
    try:
        open_connection = ConnectHandler(device_type=dev_type, ip=dev_ip, username=dev_un, password=dev_pw)
        return True
    except:
        return False

def ping_router(hostname):
    response = os.system("ping -c 1 " + hostname)
    if response == 0:
        return True
    else:
        return False

def interface_check(dev_type, dev_ip, dev_un, dev_pw):
    try:
        open_connection = ConnectHandler(device_type=dev_type, ip=dev_ip, username=dev_un, password=dev_pw)
        my_command = open_connection.send_command("show ip in brief")
    except:
        my_command = "** ISSUING COMMAND FAILED **"
    finally:
        return my_command

def main():
    start_time = datetime.now()

    file_location = input("\nInput Interface matrix (.csv, .xls) file location: ")
    entry = retv_excel(file_location)

    print("\n***** BEGIN SSH CHECKING *****")
    for x in entry.keys():
        if login_router(entry[x], x, "admin", "alta3"):
            print("\n\t**IP: - " + x + " - SSH connectivity UP\n")
        else:
            print("\n\t**IP: - " + x + " - SSH connectivity DOWN\n")

    print("\n***** BEGIN ICMP CHECKING *****")
    for x in entry.keys():
        if ping_router(x):
            print("\n\t**IP: - " + x + " - responding to ICMP\n")
        else:
            print("\n\t**IP: - " + x + " - NOT responding to ICMP\n")

    print("\n***** BEGIN SHOW IP INT BRIEF *****")
    for x in entry.keys():
        print ("\n" + interface_check(entry[x], x, "admin", "alta3"))

    #print("\n***** BEGIN ARP ENTRY CHECKING *****")
    #for x in entry.keys():
        #net_connect = ConnectHandler(entry[x], x, "admin", "alta3")
        #output = net_connect.send_command("show arp")
        #print("\n\t**----- Device " + x + " -----\n")
        #print(output)
        #print("----- END -----")

    print("\n***** BEGIN BOOTSTRAPING CHECKING *****")
    ynchk = input("\nWould you like to apply a new configuration? y/N ")
    if (ynchk.lower() == "y") or (ynchk.lower() == "yes"):
        conf_loc = input("\nWhere is the loction of the new config file? ")
        conf_ip = input("\nWhat is the hostname of the device to be configured? ")

        if bootstrapper.bootstrapper(entry[conf_ip], conf_ip, "admin", "alta3", conf_loc):
            print("\nNew configuration applied!")

            ynchk = input("\nWould you like to apply a new configuration? y/N ")
            if (ynchk.lower() == "y") or (ynchk.lower() == "yes"):
                conf_loc = input("\nWhere is the loction of the new config file? ")
                conf_ip = input("\nWhat is the hostname of the device to be configured? ")

                if bootstrapper.bootstrapper(entry[conf_ip], conf_ip, "admin", "alta3", conf_loc):
                    print("\nNew configuration applied!")
                else:
                    print("\nProblem in apply new configuration!")

if __name__ == "__main__":
    main()
