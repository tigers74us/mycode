# python3 -m pip install --user netmiko
from netmiko import ConnectHandler

def bootstrapper(dev_type, dev_ip, dev_un, dev_pw, config):
    try:
        config_file = open(config)
        config_lines = config_file.read().splitlines()
        config_file.close()

        open_connection = ConnectHandler(device_type=dev_type, ip=dev_ip, username=dev_un, password=dev_pw)
        # Established a persistence connection
        open_connection.enable()

        output = open_connection.send_config_set(config_lines)
        print(output)

        open_connection.send_command_expect('write memory')
        # Close persistent connection
        open_connection.disconnect()

        return True

    except:
        return False

