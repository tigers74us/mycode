#!/usr/bin/python3
"""Alta3 Research - tracking ISS updated output"""

import requests
import json

MAJORTOM = "http://api.open-notify.org/astros.json"

def main():
    """reading json from api"""
    # Create a urllib.request response object by sending an HTTP GET to URL
    helmetson = requests.get(MAJORTOM).json()

    ## display our Pythonic data
    print("\n\nConverted Python data")
    print(helmetson)  

    print('\n\nPeople in Space: ', helmetson['number'])
    people = helmetson['people']
    print(people)


if __name__ == "__main__":
    main()
