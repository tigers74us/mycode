#!/usr/bin/python3
"""Alta3 Research - tracking ISS updated output"""

import requests
import json

MAJORTOM = "http://api.open-notify.org/astros.json"

def main():
    """reading json from api"""
    # Create a urllib.request response object by sending an HTTP GET to URL
    helmetson = requests.get(MAJORTOM).json()

    # display people in space
    print("People in space: " + str(helmetson["number"]))

    # display every item in a list
    for astro in helmetson["people"]:
        # display ONLY the name value associated with astro
        print(astro["name"] + " on the " + astro["craft"])

if __name__ == "__main__":
    main()
