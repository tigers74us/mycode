#!/usr/bin/env python3

import csv

def main():
    with open('superbirthday.csv') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print('Column names are {}'.format(", ".join(row)))
                line_count += 1
            else:
                print('\t{} aka {}, was born in {}.'.format(row["name"],row["heroname"],row["birthday month"]))
                line_count += 1
        print('Proccessed {} lines.'.format(line_count))

if __name__ == "__main__":
    main()
    