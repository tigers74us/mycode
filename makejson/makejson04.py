#!/usr/bin/python3

import json

def main():
    """runtime code"""
    with open("datacenter.json", "r") as datacenter:
      datacenterdecoded = json.load(datacenter)

    ## This is now a dictionary
    print(type(datacenterdecoded))

    ## Display the servers in the datacenter
    print(datacenterdecoded)

    ## display the servers in row3
    print(datacenterdecoded["row3"])

    ## display the 2nd server in row2
    print(datacenterdecoded["row2"][1])

if __name__ == "__main__":
    main()
    