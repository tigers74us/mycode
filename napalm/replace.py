from napalm import get_network_driver # import code from NAPALM
driver = get_network_driver('eos') # get the driver for Arista devices
device = driver('sw-1', 'admin', 'alta3') # apply the switch credentials
device.open() # start the connection
#The next two lines of code are new
device.load_replace_candidate("/home/student/mycode/napalm/sw1.conf")
device.commit_config()
device.close() # close the connection