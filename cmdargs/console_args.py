#!/usr/bin/env python3
import argparse
import socket
from datetime import datetime

def Xbox(location):
    x = "Your choice was Xbox and it will installed at " + str(location)
    return x

def Nintendo(location):
    x = "Your choice was Nintendo and it will be installed at " + str(location)
    return x

def PlayStation(location):
    x = "Your choice was PlayStation and it will be installed at " + str(location)
    return x

if __name__ == '__main__':
    choices = {'Xbox': Xbox, 'Switch': Nintendo, 'PlayStation': PlayStation}
    parser = argparse.ArgumentParser(description='Console in the house')
    parser.add_argument('role', choices=choices, help='Required: Choose Xbox, Nintendo, or PlayStation')
    parser.add_argument('-l', metavar='location', type=str, default="Living Room",
                        help='location (Living Room)')
    args = parser.parse_args()
    function = choices[args.role]
    print(function(args.l))
