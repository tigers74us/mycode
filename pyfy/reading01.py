#!/usr/bin/env python3

def main():
    """Code that will run at Runtime"""
    myfile = open("vendor.txt",'r')
    with open('vendor-ips.txt','w') as myoutfile:
        for line in myfile.readlines():
            splitline = line.split(' ')
            myoutfile.write(splitline[-1])
    myfile.close()

if __name__ == "__main__":
    main()