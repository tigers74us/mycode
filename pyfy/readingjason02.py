#!/usr/bin/env python3
import json
def main():
    with open("ciscoex.json", "r") as myfile:
        myjson = json.load(myfile)
    with open("ciscoex.text","w") as myfile:
        myfile.write(str(myjson["time"]) + " " + myjson["host"] + " " + myjson["type"] + '\n')
if __name__ == "__main__":
    main()